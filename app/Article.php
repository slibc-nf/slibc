<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $table = 'artikel';
    protected $fillable = [
        'title', 'image','body' , 'author', 'category_id'
    ];

}
