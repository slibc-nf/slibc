<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.categories.index');
    }

    public function create_category(Request $request)
    {
        // try cath block
        try {
            // set varibale
            $name = $request->name;
            $created_at = Carbon::now();
            // add data to database
            $doAdd = DB::table('category')->insert([
                'name'          => $name,
                'created_at'    => $created_at
            ]);
            // validation if data error
            if (!$doAdd) throw new Exception('Storing Data Failed');
            // success response
            $apiResult = [
                'meta' => [
                    'code' => '200_002',
                    'message' => "Success: berhasil tambah data"
                ]
            ];
            // show response
            return (new Response($apiResult, 200));
        } catch (Exception $e) {
            // err response
            $apiResult = [
                'meta' => [
                    'code' => '400',
                    'message' => "Error: " . $e
                ]
            ];
            // show response
            return (new Response($apiResult, 400));
        }

    }

    public function update_category(Request $request)
    {
        // try cath block
        try {
            // set variale
            $id = $request->id;
            $name = $request->name;
            $updated_at = Carbon::now();

            // update category
            $doChange = DB::table('category')->where('id', $id)->update(['name' => $name, 'updated_at' => $updated_at]);
            // validation if data error
            if (!$doChange) throw new Exception('Failed to Change Data');
            // success response
            $apiResult = [
                'meta' => [
                    'code' => '200_002',
                    'message' => "Success: berhasil ubah data"
                ]
            ];
            // show response
            return (new Response($apiResult, 200));
        } catch (Exception $e) {
            // err response
            $apiResult = [
                'meta' => [
                    'code' => '400',
                    'message' => "Error: " . $e
                ]
            ];
            // show response
            return (new Response($apiResult, 400));
        }
    }

    public function delete_category(Request $request)
    {
        // try catch block
        try {
            // set variable
            $id = $request->id;
            $deleted_at = Carbon::now();

            // delete category on db
            $doDel = DB::table('category')->where('id', $id)->update(['deleted_at' => $deleted_at]);
            // validation if data error
            if (!$doDel) throw new Exception('Failed to Delete Data');
            // success function
            $apiResult = [
                'meta' => [
                    'code' => '200_002',
                    'message' => "Success: berhasil hapus data"
                ]
            ];
            // show response
            return (new Response($apiResult, 200));
        } catch (Exception $e) {
            // err response
            $apiResult = [
                'meta' => [
                    'code' => '400',
                    'message' => "Error: " . $e
                ]
            ];
            // show response
            return (new Response($apiResult, 400));
        }
    }

    public function list_category(Request $request)
    {
        // try catch block
        try {
            $doGet = DB::select('select ctr.id ,ctr.name, (select count(id) from artikel as art where find_in_set(ctr.id, art.category_id)) as jumlah from category as ctr where ctr.deleted_at is null');
            // success response
            $apiResult = [
                'meta' => [
                    'code' => '200_002',
                    'message' => "Success: berhasil hapus data"
                ],
                'data' =>$doGet
            ];
            // show response
            return (new Response($apiResult, 200));
        } catch (Exception $e) {
            // err response
            $apiResult = [
                'meta' => [
                    'code' => '400',
                    'message' => "Error: " . $e
                ]
            ];
            // show response
            return (new Response($apiResult, 400));
        }
    }

    public function fetch_category(Request $request)
    {
        // try catch block
        try {
            // set variable
            $id = $request->id;
            // fetch data
            $doFetch = DB::table('category')->where('id', $id)->select('id','name')->get();
            // validation if error data
            if (!$doFetch) throw new Exception('Failed to Delete Data');
            // success response
            $apiResult = [
                'meta' => [
                    'code' => '200_002',
                    'message' => "Success: berhasil mendapatkan data"
                ],
                'data' => $doFetch[0]
            ];
            // show response
            return (new Response($apiResult, 200));
        } catch (Exception $e) {
            // err response
            $apiResult = [
                'meta' => [
                    'code' => '400',
                    'message' => "Error: " . $e
                ]
            ];
            // show response
            return (new Response($apiResult, 400));
        }
    }

}
