<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Event;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.events.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.events.create');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin.events.edit');   
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function create_event(Request $req){
        try {

            $event = Event::insert([
                'agenda'        => $req->agenda,
                'poster'        => $req->poster,
                'start_date'    => $req->start_date,
                'end_date'      => $req->end_date,
                'description'   => $req->description,
                'location'      => $req->location,
                'created_at'    => Carbon::now()
            ]);

            $apiRes = [
                'meta' => [
                    'code' => '200_002',
                    'message' => 'Data berhasil di tambahkan'
                ],
                'data' => $event
            ];

            return (new Response($apiRes, 200));

        } catch (\Exception $e) {
            $apiRes = [
                'meta' => [
                    'code' => '400_000',
                    'message' => 'Unknown Error:'.$e->getMessage()
                ]
            ];
            return (new Response($apiRes, 400));
        }
    }

    public function fetch_event(Request $req){
        try {
            $id = $req->id;

            $event = Event::where('id', $id)->select('id', 'agenda', DB::raw('date_format(start_date, "%Y-%m-%d") as start_date'), DB::raw('date_format(end_date, "%Y-%m-%d")as end_date'), 'description', 'location')->get();

            $apiRes = [
                'meta' => [
                    'code' => '200_002 ',
                    'message' => 'Data berhasil di tampilkan'
                ],
                'data' =>$event
            ];
            return response($apiRes, 200);
        } catch (\Exception $e) {
            $apiRes = [
                'meta' => [
                    'code' => '400_000 ',
                    'message' => 'Unknown Error:  '.$e->getMessage()
                ]
            ];
            return (new Response($apiRes, 400));
        }
    }

    public function show_event(Request $req){
        try {

            // $artikel = Article::where('deleted_at', '=', null)->get();
            $event = Event::where('deleted_at', '=', null)->select('id', 'agenda', DB::raw('date_format(start_date, "%Y-%m-%d") as start_date'), DB::raw('date_format(end_date, "%Y-%m-%d")as end_date'), 'description', 'location')->get();

            $apiRes = [
                'meta' => [
                    'code' => '200_002 ',
                    'message' => 'Data berhasil di tampilkan'
                ],
                'data' =>$event
            ];
            return response($apiRes, 200);
        } catch (\Exception $e) {
            $apiRes = [
                'meta' => [
                    'code' => '400_000 ',
                    'message' => 'Unknown Error:  '.$e->getMessage()
                ]
            ];
            return (new Response($apiRes, 400));
        }
    }

    public function update_event(Request $req){
        try {

            $id = $req->id;
            $agenda = $req->agenda;
            $poster = $req->poster;
            $start_date = $req->start_date;
            $end_date = $req->end_date;
            $description = $req->description;
            $location = $req->location;
            $updated_at = Carbon::now();

            $event = Event::find($id)->update([
                'agenda'        => $agenda,
                // 'poster'        => $poster,
                'start_date'    => $start_date,
                'end_date'      => $end_date,
                'description'   => $description,
                'location'      => $location,
                'updated_at'    => $updated_at
            ]);

            $apiRes = [
                'meta' => [
                    'code' => '200_002',
                    'message' => "Data berhasil di update"
                ],
                'data' => $event
            ];

            return (new Response($apiRes, 200));
        } catch (\Throwable $e) {
            $apiRes = [
                'meta' => [
                    'code' => '400_000 ',
                    'message' => 'Unknown Error:  '.$e->getMessage()
                ]
            ];
            return (new Response($apiRes, 400));
        }
    }
    public function delete_event(Request $req){

        try {
            $id = $req->id;

            $event = Event::find($id);
            $event->deleted_at = Carbon::now();
            $event->save();

            // dd($artikel);

            $apiRes = [
                'meta' => [
                    'code'=>'200_002',
                    'message' => "Data berhasil di hapus"
                ],
                'data'=>$event
            ];

            return (new Response($apiRes, 200));

        } catch (\Exception $e) {
            $apiRes = [
                'meta' => [
                    'code' => '400_000',
                    'message' => 'Unknown Error: '.$e->getMessage()
                ]
            ];
            return (new Response($apiRes, 400));
        }
    }
}
