<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function index()
    {
        return view('admin.login');
    }

    public function login()
    {
        $this->validate(request(), [
            'username' => 'required',
            'password' => 'required',
        ]);

        $credentials = request()->only('username', 'password');

        if (!Auth::attempt($credentials)) {
            return back()->withError('username or password is wrong');
        }

        return redirect()->intended('admin/dashboard');
    }

    public function logout()
    {
        auth()->logout();

        return redirect()->route('login');
    }
}
