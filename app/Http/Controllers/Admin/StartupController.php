<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class StartupController extends Controller
{
    public function index()
    {
        return view('admin.startup.index');
    }

    public function store()
    {
    }

    public function create()
    {
        return view('admin.startup.create');
    }
}
