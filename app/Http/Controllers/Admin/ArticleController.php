<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Carbon\Carbon;
use App\Article;
use Illuminate\Support\Facades\DB;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.articles.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.articles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return view('admin.articles.edit');
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.articles.create');
        dd(2);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function create_article(Request $request){
        try {

            $title = $request->title;
            $body = $request->body;
            $image = $request->image;
            $author = $request->author;
            $category_id = $request->category_id;
            $created_at = Carbon::now();

            $artikel = Article::insert([
                'category_id'   => $category_id,
                'title'         => $title,
                'image'         => $image,
                'body'          => $body,
                'author'        => $author,
                'created_at'    => $created_at
            ]);

            $apiRes = [
                'meta' => [
                    'code' => '200_002',
                    'message' => 'Data berhasil di tambahkan'
                ],
                'data' => $artikel
            ];

            return (new Response($apiRes, 200));

        } catch (\Exception $e) {
            $apiRes = [
                'meta' => [
                    'code' => '400_000',
                    'message' => 'Unknown Error:'.$e->getMessage()
                ]
            ];
            return (new Response($apiRes, 400));
        }
    }

    public function fetch_artikel(Request $req){
        try {
            $id = $req->id;

            $artikel = Article::addSelect(['category name' => function ($query){
                $query->select('name')
                ->from('category')
                ->whereColumn('id', 'artikel.category_id')
                ->limit(1);
            }])->where('id', $id)->get();

            $apiRes = [
                'meta' => [
                    'code' => '200_002 ',
                    'message' => 'Data berhasil di tampilkan'
                ],
                'data' =>$artikel
            ];
            return response($apiRes, 200);
        } catch (\Exception $e) {
            $apiRes = [
                'meta' => [
                    'code' => '400_000 ',
                    'message' => 'Unknown Error:  '.$e->getMessage()
                ]
            ];
            return (new Response($apiRes, 400));
        }
    }

    public function show_artikel(Request $req){
        try {

            // $artikel = Article::where('deleted_at', '=', null)->get();
            $artikel = Article::addSelect(['category_name' => function ($query){
                $query->select('name')
                ->from('category')
                ->whereColumn('id', 'artikel.category_id')
                ->limit(1);
            }])->where('deleted_at', '=', null)->get();

            $apiRes = [
                'meta' => [
                    'code' => '200_002 ',
                    'message' => 'Data berhasil di tampilkan'
                ],
                'data' =>$artikel
            ];
            return response($apiRes, 200);
        } catch (\Exception $e) {
            $apiRes = [
                'meta' => [
                    'code' => '400_000 ',
                    'message' => 'Unknown Error:  '.$e->getMessage()
                ]
            ];
            return (new Response($apiRes, 400));
        }
    }

    public function update_artikel(Request $req){
        try {

            $category_id = $req->category_id;
            $id = $req->id;
            $title = $req->title;
            $image = $req->image;
            $body = $req->body;
            $author = $req->author;
            $updated_at = Carbon::now();

            // dd($image);

            $artikel = Article::find($id)->update([
                'category_id'   => $category_id,
                'title'         => $title,
                'image'         => $image,
                'body'          => $body,
                'author'        => $author,
                'updated_at'    => $updated_at
            ]);

            $apiRes = [
                'meta' => [
                    'code' => '200_002',
                    'message' => "Data berhasil di update"
                ],
                'data' => $artikel
            ];

            return (new Response($apiRes, 200));
        } catch (\Exception $e) {
            $apiRes = [
                'meta' => [
                    'code' => '400_000',
                    'message' => 'Unknown Error: '.$e->getMessage()
                ]
            ];
            return (new Response($apiRes, 400));
        }
    }
    public function delete_artikel(Request $req){

        try {
            $id = $req->id;

            $artikel = Article::find($id);
            $artikel->deleted_at = Carbon::now();
            $artikel->save();

            // dd($artikel);

            $apiRes = [
                'meta' => [
                    'code'=>'200_002',
                    'message' => "Data berhasil di hapus"
                ],
                'data'=>$artikel
            ];

            return (new Response($apiRes, 200));

        } catch (\Exception $e) {
            $apiRes = [
                'meta' => [
                    'code' => '400_000',
                    'message' => 'Unknown Error: '.$e->getMessage()
                ]
            ];
            return (new Response($apiRes, 400));
        }
    }

}
