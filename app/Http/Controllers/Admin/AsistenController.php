<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Carbon\Carbon;
use App\AsistenLecturer;

class AsistenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function fetch_asdos(Request $req){
        try {
            $id = $req->id;

            $asdos = AsistenLecturer::where('id',$id)->get();

            $apiRes = [
                'meta' => [
                    'code' => '200_002 ',
                    'message' => 'Data berhasil di tampilkan'
                ],
                'data' =>$asdos
            ];
            return response($apiRes, 200);
        } catch (\Exception $e) {
            $apiRes = [
                'meta' => [
                    'code' => '400_000 ',
                    'message' => 'Unknown Error:  '.$e->getMessage()
                ]
            ];
            return (new Response($apiRes, 400));
        }
    }
    public function show_asdos(Request $req){
        try {

            $asdos = AsistenLecturer::where('deleted_at',null)->get();

            $apiRes = [
                'meta' => [
                    'code' => '200_002 ',
                    'message' => 'Data berhasil di tampilkan'
                ],
                'data' =>$asdos
            ];
            return response($apiRes, 200);

        } catch (\Exception $e) {
            $apiRes = [
                'meta' => [
                    'code' => '400_000 ',
                    'message' => 'Unknown Error:  '.$e->getMessage()
                ]
            ];
            return (new Response($apiRes, 400));
        }
    }

    public function create_asdos(Request $req){
        try {
            $name = $req->name;
            $phone = $req->phone;
            $email = $req->email;
            $matkul = $req->matkul;
            $dosen_name = $req->dosen_name;
            $created_at = Carbon::now();

            $asdos = AsistenLecturer::insert([
                'name'          => $name,
                'phone'         => $phone,
                'email'         => $email,
                'matkul'        => $matkul,
                'dosen_name'    => $dosen_name,
                'created_at'    => Carbon::now()
            ]);

            // dd($asdos);

            $apiRes = [
                'meta'=> [
                    'code' => '200_002',
                    'message' => 'Berhasil menambahkan data'
                ],
                'data' =>$asdos
            ];

            return (new Response($apiRes, 200));

        } catch (\Exception $e) {
            $apiRes = [
                'meta' => [
                    'code' => '400_000',
                    'message' => 'Unknown Error: '.$e->getMessage()
                ]
            ];
            return (new Response($apiRes, 400));
        }
    }
    public function delete_asdos(Request $req){
        try {
            $id = $req->id;

            $asdos = AsistenLecturer::find($id);
            $asdos->deleted_at = Carbon::now();
            $asdos->save();

            $apiRes = [
                'meta'=> [
                    'code' => '200_002',
                    'message' => 'data berhasil dihapus'
                ],
                'data'=>$asdos
            ];
            return response($apiRes, 200);
        } catch (\Throwable $th) {
            $apiRes = [
                'meta' => [
                    'code' => '400_000',
                    'message' => 'Unknown Error: '.$e->getMessage()
                ]
            ];
            return (new Response($apiRes, 400));
        }
    }

    public function update_asdos(Request $req){
        try {
            $id = $req->id;

            $asdos = AsistenLecturer::find($id)->update([
                'name'          => $req->name,
                'phone'         => $req->phone,
                'email'         => $req->email,
                'matkul'        => $req->matkul,
                'dosen_name'    => $req->dosen_name,
                'updated_at'    => Carbon::now()
            ]);

            $apiRes = [
                'meta' => [
                    'code' => '200_002',
                    'message' => "Data berhasil di update"
                ],
                'data' => $asdos
            ];

            return (new Response($apiRes, 200));

        } catch (\Exception $e) {
            $apiRes = [
                'meta' => [
                    'code' => '400_000',
                    'message' => 'Unknown Error: '.$e->getMessage()
                ]
            ];
            return (new Response($apiRes, 400));
        }
    }
}
