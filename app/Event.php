<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $table = 'event';
    protected $fillable = [
        'agenda', 'poster' , 'start_date', 'end_date','description','location'
    ];
    // public $timestamps = false;
}
