<footer id="footer">
    <div class="container">
        <div class="row d-flex align-items-center">
            <div class="col-lg-6 text-lg-left text-center">
                <div class="copyright">
                    &copy; Created By <strong>STT NF students</strong>
                </div>
            </div>
        </div>
    </div>
</footer>
