<header id="header" class="fixed-top d-flex align-items-center">
    <div class="container d-flex align-items-center">

        <div class="logo mr-auto">
            <h1 class="text-light"><a href="/"><img src="assets/img/slibc.png" alt=""></a></h1>
            <!-- Uncomment below if you prefer to use an image logo -->
            <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
        </div>

        <nav class="nav-menu d-none d-lg-block">
            <ul>
                <li class="active"><a href="#index.html">Home</a></li>
                <li><a href="#about">About</a></li>
                <li><a href="#services">Programs</a></li>
                <!-- <li><a href="#portfolio">Portfolio</a></li> -->
                <li><a href="#team">Informasi</a></li>
                <li><a href="#contact">Contact</a></li>

                <!-- <li class="get-started"><a href="#about">Get Started</a></li> -->
            </ul>
        </nav><!-- .nav-menu -->

    </div>
</header>
