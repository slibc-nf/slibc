@extends('layouts.admin.main')

@section('title', 'Create Category SLIBC')

@section('content')
<div class="col-xl-8 order-xl-1 mx-auto">
    <div class="card">
        <div class="card-header">
            <div class="row align-items-center">
                <div class="col-6">
                    <h3 class="mb-0" id="label-opt">Buat Kategori</h3>
                </div>
                <div class="col-6" id="create-opt-hide" hidden="true">
                    <div class="d-flex justify-content-end display-0">
                        <button class="btn btn-primary" id="opt-create">Tambah Data</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body">
            <form>
                <!-- Address -->
                <div class="pl-lg-4">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="form-control-label" for="title">Kategori</label>
                                <input type="text" class="form-control" id="upt-category" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pl-lg-4">
                    <div class="form-group">
                        <button type="button" id="btn-update" class="btn btn-default btn-block display-0">Save Data Edit</button>
                        <button type="button" id="btn-create" class="btn btn-default btn-block">Save Data Tambah</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="col-xl-8 order-xl-1 mx-auto">
    <div class="card">
        <div class="card-header">
            <div class="row align-items-center">
                <div class="col-8">
                    <h3 class="mb-0">List Kategori</h3>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-flush" id="list-category">
                    <thead class="thead-light">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Kategori</th>
                            <th scope="col">Artikel</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody id="list">
                        {{-- <tr>
                            <td>
                                1
                            </td>
                            <td>
                                Startup
                            </td>
                            <td>
                                <span class="badge badge-pill badge-primary">2</span>
                            </td>
                            <td class="text-right">
                                <div class="dropdown">
                                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-ellipsis-v"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                        <a class="dropdown-item" href="#">Edit</a>
                                        <a class="dropdown-item" href="#">Delete</a>
                                    </div>
                                </div>
                            </td>
                        </tr> --}}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="/assets/js/category/category.js"></script>
@endsection
