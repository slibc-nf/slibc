@extends('layouts.admin.main')

@section('title', 'Article SLIBC')

@section('content')
<div class="col">
    <div class="card">
        <!-- Card header -->
        <div class="card-header border-0">
            <div class="row align-items-center">
                <div class="col-8">
                    <h3 class="mb-0">List Article</h3>
                </div>
                <div class="col-4 text-right text-white">
                    <a class="btn btn-default btn-sm" href="{{ route('articles.create') }}">Buat artikel</a>
                </div>
            </div>
        </div>
        <!-- Light table -->
        <div class="table-responsive">
            <table class="table table-flush" id="table-category">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">Gambar</th>
                        <th scope="col">Penulis</th>
                        <th scope="col">Judul</th>
                        <th scope="col">Kategori</th>
                        <th scope="col">Content</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody class="list" id="list_category">
                    {{-- <tr>
                        <th scope="row">
                            <div class="media align-items-center">
                                <a href="#" class="avatar rounded-circle mr-3">
                                    <img alt="Image placeholder" src="/admin/assets/img/theme/bootstrap.jpg">
                                </a>
                            </div>
                        </th>
                        <td class="title">
                            Fajar Dwi Saputra
                        </td>
                        <td class="title">
                            Lorem ipsum dolor sit, amet consectetur adipisicing elit
                        </td>
                        <td class="category">
                            PKM
                        </td>
                        <td class="content">
                            Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        </td>
                        <td class="text-right">
                            <div class="dropdown">
                                <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-ellipsis-v"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                    <a class="dropdown-item" href="#">View</a>
                                    <a class="dropdown-item" href="#">Edit</a>
                                    <a class="dropdown-item" href="#">Delete</a>
                                </div>
                            </div>
                        </td>
                    </tr> --}}
                    {{-- <tr>
                        <th scope="row">
                            <div class="media align-items-center">
                                <a href="#" class="avatar rounded-circle mr-3">
                                    <img alt="Image placeholder" src="/admin/assets/img/theme/bootstrap.jpg">
                                </a>
                            </div>
                        </th>
                        <td class="title">
                            M. Fais Albaya
                        </td>
                        <td class="budget">
                            Lorem ipsum dolor sit, amet consectetur adipisicing elit
                        </td>
                        <td>
                            PKM
                        </td>
                        <td>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit
                        </td>
                        <td class="text-right">
                            <div class="dropdown">
                                <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-ellipsis-v"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                    <a class="dropdown-item" href="#">View</a>
                                    <a class="dropdown-item" href="#">Edit</a>
                                    <a class="dropdown-item" href="#">Delete</a>
                                </div>
                            </div>
                        </td>
                    </tr> --}}
                </tbody>
            </table>
        </div>
    </div>
</div>


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="/assets/js/artikel/list.js"></script>
@endsection
