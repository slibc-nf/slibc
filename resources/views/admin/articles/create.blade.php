@extends('layouts.admin.main')

@section('title', 'Create Article SLIBC')

@section('content')
<div class="col-xl-8 order-xl-1 mx-auto">
    <div class="card">
        <div class="card-header">
            <div class="row align-items-center">
                <div class="col-8">
                    <h3 class="mb-0">Buat Artikel</h3>
                </div>
            </div>
        </div>
        <div class="card-body">
            <form>
                <!-- Address -->
                <div class="pl-lg-4">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="file" class="custom-file-input" id="customFile" lang="en">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="form-control-label" for="author">Penulis</label>
                                <input id="author" class="form-control" type="text" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="form-control-label" for="title">Judul</label>
                                <input id="title" class="form-control" type="text" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="form-control-label" for="category">Kategory</label>
                                <select name="category" id="category" class="form-control" required>
                                    <option value="">Pilih Kategori</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pl-lg-4">
                    <div class="form-group">
                        <label class="form-control-label">Content</label>
                        <textarea rows="4" id="body" name="body" class="form-control" placeholder="A few words about you ..." required></textarea>
                    </div>
                </div>
                <div class="pl-lg-4">
                    <div class="form-group">
                        <button type="button" id="submit-article" class="btn btn-default btn-block">Submit form</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="/assets/js/artikel/add_article.js"></script>
@endsection
