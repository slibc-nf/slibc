@extends('layouts.admin.main')

@section('title', 'Event SLIBC')

@section('content')
<div class="col">
    <div class="card">
        <!-- Card header -->
        <div class="card-header border-0">
            <div class="row align-items-center">
                <div class="col-8">
                    <h3 class="mb-0">List Event</h3>
                </div>
                <div class="col-4 text-right text-white">
                    <a class="btn btn-default btn-sm" href="{{ route('events.create') }}">Buat event</a>
                </div>
            </div>
        </div>
        <!-- Light table -->
        <div class="table-responsive">
            <table class="table table-flush" id="table-event">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">Event</th>
                        <th scope="col">Deskripsi</th>
                        <th scope="col">Tanggal Mulai</th>
                        <th scope="col">Tanggal Selesai</th>
                        <th scope="col">Lokasi</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody class="list" id="list">
                    {{-- <tr>
                        <td class="event">
                            Lorem ipsum dolor sit, amet consectetur adipisicing elit
                        </td>
                        <td class="description">
                            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Adipisci, recusandae.
                        </td>
                        <td class="date">
                            10-01-2021
                        </td>
                        <td class="date">
                            10-01-2021
                        </td>
                        <td class="location">
                            Lorem ipsum dolor sit amet.
                        </td>
                        <td class="text-right">
                            <div class="dropdown">
                                <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-ellipsis-v"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                    <a class="dropdown-item" href="#">Edit</a>
                                    <a class="dropdown-item" href="#">Delete</a>
                                </div>
                            </div>
                        </td>
                    </tr> --}}
                </tbody>
            </table>
        </div>
    </div>
</div>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="/assets/js/event/list.js"></script>
@endsection
