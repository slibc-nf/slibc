@extends('layouts.admin.main')

@section('title', 'Edit Event SLIBC')

@section('content')
<div class="col-xl-8 order-xl-1 mx-auto">
    <div class="card">
        <div class="card-header">
            <div class="row align-items-center">
                <div class="col-8">
                    <h3 class="mb-0">Edit Event</h3>
                </div>
            </div>
        </div>
        <div class="card-body">
            <form>
                <!-- Address -->
                <div class="pl-lg-4">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="form-control-label" for="event">Event</label>
                                <input id="event" class="form-control" type="text" value="Lorem Ipsum">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="form-control-label" for="description">Deskripsi</label>
                                <textarea id="description" cols="30" rows="2" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pl-lg-4">
                    <div class="form-group">
                        <label class="form-control-label" for="date-start">Tanggal Mulai</label>
                        <input id="date-start" class="form-control" type="date">
                    </div>
                </div>
                <div class="pl-lg-4">
                    <div class="form-group">
                        <label class="form-control-label" for="date-end">Tanggal Selesai</label>
                        <input id="date-end" class="form-control" type="date">
                    </div>
                </div>
                <div class="pl-lg-4">
                    <div class="form-group">
                        <label class="form-control-label" for="location">Lokasi</label>
                        <input id="location" class="form-control" type="location" value="STT-NF">
                    </div>
                </div>
                <div class="pl-lg-4">
                    <div class="form-group">
                        <button type="button" id="edit-event" class="btn btn-default btn-block">Submit form</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="/assets/js/event/edit_event.js"></script>
@endsection
