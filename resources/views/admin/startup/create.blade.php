@extends('layouts.admin.main')

@section('title', 'Create Startup SLIBC')

@section('content')
<div class="col-xl-8 order-xl-1 mx-auto">
    <div class="card">
        <div class="card-header">
            <div class="row align-items-center">
                <div class="col-8">
                    <h3 class="mb-0">Buat Startup</h3>
                </div>
            </div>
        </div>
        <div class="card-body">
            <form>
                <!-- Address -->
                <div class="pl-lg-4">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="form-control-label" for="#">Lorem</label>
                                <input id="#" class="form-control" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="form-control-label" for="#">Lorem</label>
                                <input id="#" class="form-control" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="form-control-label" for="#">Lorem</label>
                                <input id="#" class="form-control" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="form-control-label" for="#">Lorem</label>
                                <input id="#" class="form-control" type="text">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pl-lg-4">
                    <div class="form-group">
                        <button type="button" class="btn btn-default btn-block">Submit form</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
