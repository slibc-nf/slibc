@extends('layouts.admin.main')

@section('title', 'List Startup SLIBC')

@section('content')
<div class="col mx-auto">
    <div class="card">
        <div class="card-header">
            <div class="row align-items-center">
                <div class="col-8">
                    <h3 class="mb-0">List Startup</h3>
                </div>
                <div class="col-4 text-right text-white">
                    <a class="btn btn-default btn-sm" href="{{ route('startups.create') }}">Buat startup</a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-flush">
                    <thead class="thead-light">
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Name</th>
                            <th scope="col">Lorem</th>
                            <th scope="col">Lorem</th>
                            <th scope="col">Lorem</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody class="list">
                        <tr>
                            <td>
                                1
                            </td>
                            <td>
                                Lorem ipsum dolor sit.
                            </td>
                            <td>
                                Lorem, ipsum dolor.
                            </td>
                            <td>
                                Lorem, ipsum dolor.
                            </td>
                            <td>
                                Lorem, ipsum dolor.
                            </td>
                            <td class="text-right">
                                <div class="dropdown">
                                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-ellipsis-v"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                        <a class="dropdown-item" href="#">Edit</a>
                                        <a class="dropdown-item" href="#">Delete</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                2
                            </td>
                            <td>
                                Lorem ipsum dolor.
                            </td>
                            <td>
                                Lorem, ipsum dolor.
                            </td>
                            <td>
                                Lorem, ipsum dolor.
                            </td>
                            <td>
                                Lorem, ipsum dolor.
                            </td>
                            <td class="text-right">
                                <div class="dropdown">
                                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-ellipsis-v"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                        <a class="dropdown-item" href="#">Edit</a>
                                        <a class="dropdown-item" href="#">Delete</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                3
                            </td>
                            <td>
                                Lorem, ipsum dolor.
                            </td>
                            <td>
                                Lorem ipsum dolor sit.
                            </td>
                            <td>
                                Lorem ipsum dolor sit.
                            </td>
                            <td>
                                Lorem ipsum dolor sit.
                            </td>
                            <td class="text-right">
                                <div class="dropdown">
                                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-ellipsis-v"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                        <a class="dropdown-item" href="#">Edit</a>
                                        <a class="dropdown-item" href="#">Delete</a>
                                    </div>
                                </div>
                            </td>
                        </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
