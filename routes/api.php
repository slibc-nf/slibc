<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//login
Route::get('login', 'Admin\AuthController@login');

//articles
Route::post('create_article', 'admin\ArticleController@create_article');
Route::get('show_article', 'admin\ArticleController@show_artikel');
Route::post('update_artikel', 'admin\ArticleController@update_artikel');
Route::post('delete_artikel', 'admin\ArticleController@delete_artikel');
Route::post('fetch_artikel', 'admin\ArticleController@fetch_artikel');

//asdos
Route::post('fetch_asdos', 'admin\AsistenController@fetch_asdos');
Route::get('show_asdos', 'admin\AsistenController@show_asdos');
Route::post('create_asdos', 'admin\AsistenController@create_asdos');
Route::post('delete_asdos', 'admin\AsistenController@delete_asdos');
Route::post('update_asdos', 'admin\AsistenController@update_asdos');

// category
Route::post('category/add_category', 'admin\CategoryController@create_category');
Route::post('category/update_category', 'admin\CategoryController@update_category');
Route::post('category/delete_category', 'admin\CategoryController@delete_category');
Route::post('category/fetch_category', 'admin\CategoryController@fetch_category');
Route::get('category/list_category', 'admin\CategoryController@list_category');


//event
Route::post('create_event', 'admin\EventController@create_event');
Route::get('show_event', 'admin\EventController@show_event');
Route::post('update_event', 'admin\EventController@update_event');
Route::post('delete_event', 'admin\EventController@delete_event');
Route::post('fetch_event', 'admin\EventController@fetch_event');
