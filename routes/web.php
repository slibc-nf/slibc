<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/admin/login', 'Admin\AuthController@index')->name('login');
Route::post('/admin/login', 'Admin\AuthController@login');
Route::post('/admin/logout', 'Admin\AuthController@logout')->name('logout');

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
    Route::get('/dashboard', 'Admin\DashboardController@index')->name('admin.dashboard');
    Route::resource('/tags', 'Admin\TagController');
    Route::resource('/categories', 'Admin\CategoryController');
    Route::resource('/articles', 'Admin\ArticleController');
    Route::resource('/articles/edit', 'Admin\ArticleController@edit');
    Route::resource('/events', 'Admin\EventController');
    Route::resource('/pkm', 'Admin\PkmController');
    Route::resource('/asisten-lecturer', 'Admin\AsistenController');
    Route::resource('/startups', 'Admin\StartupController');
});
